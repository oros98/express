const mysql = require('mysql2');
const connection = mysql.createConnection({
  user: 'root',
  database: 'cervezas',
  host:'localhost',
  password: 'root'
});

module.exports = {
    connection
}