const Users = require('../models/Users');

const index = (req, res) =>{
    Users.index(function (status,data, fields){
       res.status(status).json(data);
   });
}

const show = (req, res) =>{
    const id= req.params.id;
    Users.show(id,function (status,data, fields){
        res.status(status).json(data);
    });
}


const store = (req, res) =>{
    let objeto = new Object(); 
    objeto.nombre= req.body.name;
    objeto.role_id= req.body.role_id;
    objeto.email=req.body.email;
    objeto.email_verified_date = req.body.email_verified_date;
    objeto.password= req.body.password;
    objeto.remembered = req.body.remembered;
    Users.store(objeto,function (status,data, fields){
        res.status(status).json(data);
    });
}

const update = (req, res) =>{ 
    let objeto = new Object(); 
    objeto.nombre= req.body.name;
    objeto.role_id= req.body.role_id;
    objeto.email=req.body.email;
    objeto.email_verified_date = req.body.email_verified_date;
    objeto.password= req.body.password;
    objeto.remembered = req.body.remembered;
    objeto.id=req.params.id;
    Users.update(objeto,function (status,data, fields){
        res.status(status).json(data);
    });
}

const remove = (req, res) =>{
    const id= req.params.id;
    Users.remove(id,function (status,data, fields){
        res.status(status).json(data);
    });
}


module.exports = {
    index,
    show,
    store,
    update,
    remove
}