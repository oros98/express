const Cerveza = require('../models/Cerveza');

const index = (req, res) =>{
   const cervezas = Cerveza.index(function (status,data, fields){
       res.status(status).json(data);
   });
}

const show = (req, res) =>{
    const id= req.params.id;
    const cervezas = Cerveza.show(id,function (status,data, fields){
        res.status(status).json(data);
    });
}


const store = (req, res) =>{
    let objeto = new Object(); 
    objeto.nombre= req.body.name;
    objeto.descripcion= req.body.description;
    objeto.alcohol=req.body.alcohol;
    objeto.price = req.body.price;
    const cervezas = Cerveza.store(objeto,function (status,data, fields){
        res.status(status).json(data);
    });
}

const update = (req, res) =>{ 
    let objeto = new Object(); 
    objeto.nombre= req.body.name;
    objeto.descripcion= req.body.description;
    objeto.alcohol=req.body.alcohol;
    objeto.price = req.body.price;
    objeto.id=req.params.id;
    const cervezas = Cerveza.update(objeto,function (status,data, fields){
        res.status(status).json(data);
    });
}

const remove = (req, res) =>{
    const id= req.params.id;
    const cervezas = Cerveza.remove(id,function (status,data, fields){
        res.status(status).json(data);
    });
}


module.exports = {
    index,
    show,
    store,
    update,
    remove
}