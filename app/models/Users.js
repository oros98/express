const mysql = require('../config/dbconnection');
let connection= mysql.connection;

const index = function(callback){
    const sql= 'SELECT * FROM users';
    connection.query(sql, (err, result, fields) => {
        if(err){
            callback(500,err)
        }else{
            callback(200,result,fields);
        }
    })
}
const show = function(id, callback){
    const sql='SELECT * FROM users where id = ?';
    connection.query(sql,[id], (err, result, fields) => {
        if(err){
            callback(500,err)
        }else{
            callback(200,result,fields);
        }
     })
}
const store = function(objeto, callback){
    const sql='insert into users(name,description,alcohol,price) VALUES(? ,? ,? ,? )';

    connection.query(sql,[objeto.nombre,objeto.role_id,objeto.email,objeto.password,objeto.remembered], (err, result, fields) => {
        if(err){
            callback(500,err)
        }else{
            callback(200,result,fields);
        }
     })
}
const update = function(objeto, callback){
    const sql='update users set name = ? ,description = ?,alcohol = ?, price = ? where id = ?';
        connection.query(sql,[objeto.nombre,objeto.role_id,objeto.email,objeto.password,objeto.remembered,objeto.id], (err, result, fields) => {
        if(err){
            callback(500,err)
        }else{
            callback(200,result,fields);
        }
     })
}
const remove = function(id, callback){
    const sql='DELETE FROM users where id = ?';
    connection.query(sql,[id], (err, result, fields) => {
        if(err){
            callback(500,err)
        }else{
            callback(200,result,fields);
        }
     })
}

module.exports = {
    index,
    show,
    store,
    remove,
    update
}