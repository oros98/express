const express= require('express')
var router = express.Router()
const routerCervezas = require('./routes/cervezas')
const routerUsers = require('./routes/user.js');

router.use('/cervezas',routerCervezas);
router.use('/users',routerUsers);

  module.exports = router;
  