const express= require('express')
var router = express.Router()
const cervezasController = require("../controllers/cervezaController");
//establecemos nuestra primera ruta, mediante get.

  router.get('/', (req, res) => {
      cervezasController.index(req,res)
  })

  router.get('/:id', (req, res) => {
    cervezasController.show(req,res)

  }) 

  router.post('/', (req, res) => {
    cervezasController.store(req,res)
    })
    
router.put('/:id', (req, res) => {
    cervezasController.update(req,res)
    }) 
  
    router.delete('/:id', (req, res) => {
        cervezasController.remove(req,res)
    })
module.exports =router;
