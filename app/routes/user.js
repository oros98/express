const express= require('express')
var router = express.Router()
const userController = require("../controllers/UserController");
//establecemos nuestra primera ruta, mediante get.

  router.get('/', (req, res) => {
    userController.index(req,res)
  })

  router.get('/:id', (req, res) => {
    userController.show(req,res)

  }) 

  router.post('/', (req, res) => {
    userController.store(req,res)
    })
    
router.put('/:id', (req, res) => {
    userController.update(req,res)
    }) 
  
router.delete('/:id', (req, res) => {
        userController.remove(req,res)
    })
  module.exports =router;
